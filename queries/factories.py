import factory

from faker import Faker
from django.contrib.auth.models import User

from queries import models

fake = Faker()


class UserFactory(factory.DjangoModelFactory):
    username = factory.LazyAttribute(lambda a: 'user-{}'.format(fake.pystr()).lower())
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    email = factory.LazyAttribute(lambda a: '{}@example.com'.format(fake.pystr()).lower())
    password = factory.PostGenerationMethodCall('set_password', 'password')

    class Params:
        super_user = factory.Trait(
            is_superuser=True,
            is_staff=True,
            is_active=True,
        )

    class Meta:
        model = User


class EmployeeFactory(factory.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = models.Employee


class BossFactory(factory.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = models.Boss


class RestaurantFactory(factory.DjangoModelFactory):
    name = factory.Faker('company')
    boss = factory.SubFactory(BossFactory)

    @factory.post_generation
    def employees(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for employee in extracted:
                self.employees.add(employee)

    class Meta:
        model = models.Restaurant
