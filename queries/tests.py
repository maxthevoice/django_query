from django.db.models import Prefetch
from django.test import TestCase

from queries import factories, models


class TestQueries(TestCase):

    def test_simple_query(self):
        factories.RestaurantFactory.create_batch(2)

        with self.assertNumQueries(0):
            restaurants = models.Restaurant.objects.all()

        """
        1. SELECT * FROM "queries_restaurant"
        """
        with self.assertNumQueries(1):
            for restaurant in restaurants:
                print(restaurant)

    def test_simple_select_related_query(self):
        factories.RestaurantFactory()

        restaurants = models.Restaurant.objects.all()

        """
        1. SELECT * FROM "queries_restaurant"
        2. SELECT * FROM "queries_boss" WHERE "queries_boss"."id" = 8
        """
        with self.assertNumQueries(2):
            for restaurant in restaurants:
                print(restaurant.boss)

        restaurants = models.Restaurant.objects.select_related('boss').all()

        """
        1. SELECT * FROM "queries_restaurant" 
            INNER JOIN "queries_boss" ON ("queries_restaurant"."boss_id" = "queries_boss"."id")
        """
        with self.assertNumQueries(1):
            for restaurant in restaurants:
                print(restaurant.boss)

    def test_multiple_select_related_query(self):
        factories.RestaurantFactory()

        restaurants = models.Restaurant.objects.select_related('boss').all()

        """
        1. SELECT * FROM "queries_restaurant" 
            INNER JOIN "queries_boss" ON ("queries_restaurant"."boss_id" = "queries_boss"."id")
        2. SELECT * FROM "auth_user" WHERE "auth_user"."id" = 13
        """
        with self.assertNumQueries(2):
            for restaurant in restaurants:
                print(restaurant.boss.user)

        restaurants = models.Restaurant.objects.select_related('boss__user').all()

        """
        1. SELECT * FROM "queries_restaurant" 
            INNER JOIN "queries_boss" ON ("queries_restaurant"."boss_id" = "queries_boss"."id") 
            INNER JOIN "auth_user" ON ("queries_boss"."user_id" = "auth_user"."id")
        """
        with self.assertNumQueries(1):
            for restaurant in restaurants:
                print(restaurant.boss.user)

    def test_complex_prefetch_related_query(self):
        employee_1 = factories.EmployeeFactory()
        employee_2 = factories.EmployeeFactory()
        employee_3 = factories.EmployeeFactory()
        boss = factories.BossFactory()
        factories.RestaurantFactory.create_batch(2, boss=boss, employees=(employee_1, employee_2, employee_3))

        bosses = models.Boss.objects.select_related('user').all()

        """
        1. SELECT * FROM "queries_boss" INNER JOIN "auth_user" ON ("queries_boss"."user_id" = "auth_user"."id")
        2. SELECT * FROM "queries_restaurant" WHERE "queries_restaurant"."boss_id" = 53
        3. SELECT * FROM "queries_employee" 
            INNER JOIN "queries_restaurant_employees" ON ("queries_employee"."id" = "queries_restaurant_employees"."employee_id") 
            WHERE "queries_restaurant_employees"."restaurant_id" = 62
        4. SELECT * FROM "auth_user" WHERE "auth_user"."id" = 178
        5. SELECT * FROM "auth_user" WHERE "auth_user"."id" = 179
        6. SELECT * FROM "auth_user" WHERE "auth_user"."id" = 180
        7. SELECT * FROM "queries_employee" 
            INNER JOIN "queries_restaurant_employees" ON ("queries_employee"."id" = "queries_restaurant_employees"."employee_id") 
            WHERE "queries_restaurant_employees"."restaurant_id" = 63
        8. SELECT * FROM "auth_user" WHERE "auth_user"."id" = 178
        9. SELECT * FROM "auth_user" WHERE "auth_user"."id" = 179
        10. SELECT * FROM "auth_user" WHERE "auth_user"."id" = 180
        """
        with self.assertNumQueries(10):
            for boss in bosses:
                print(boss.user)
                for restaurant in boss.restaurant_set.all():
                    for employee in restaurant.employees.all():
                        print(employee.user)

        bosses = models.Boss.objects \
            .select_related('user') \
            .prefetch_related('restaurant_set__employees__user') \
            .all()

        """
        1. SELECT * FROM "queries_boss" INNER JOIN "auth_user" ON ("queries_boss"."user_id" = "auth_user"."id")
        2. SELECT * FROM "queries_restaurant" WHERE "queries_restaurant"."boss_id" IN (56)
        3. SELECT ("queries_restaurant_employees"."restaurant_id") AS "_prefetch_related_val_restaurant_id", * 
            FROM "queries_employee" 
            INNER JOIN "queries_restaurant_employees" ON ("queries_employee"."id" = "queries_restaurant_employees"."employee_id") 
            WHERE "queries_restaurant_employees"."restaurant_id" IN (68, 69)
        4. SELECT * FROM "auth_user" WHERE "auth_user"."id" IN (192, 190, 191)
        """
        with self.assertNumQueries(4):
            for boss in bosses:
                print(boss.user)
                for restaurant in boss.restaurant_set.all():
                    for employee in restaurant.employees.all():
                        print(employee.user)

        bosses = models.Boss.objects \
            .select_related('user') \
            .prefetch_related(
                Prefetch(
                    'restaurant_set__employees',
                    queryset=models.Employee.objects.select_related('user'),
                ),
            ).all()

        """
        1. SELECT * FROM "queries_boss" INNER JOIN "auth_user" ON ("queries_boss"."user_id" = "auth_user"."id")
        2. SELECT * FROM "queries_restaurant" WHERE "queries_restaurant"."boss_id" IN (58)
        3. SELECT ("queries_restaurant_employees"."restaurant_id") AS "_prefetch_related_val_restaurant_id", * 
            FROM "queries_employee" 
            INNER JOIN "queries_restaurant_employees" ON ("queries_employee"."id" = "queries_restaurant_employees"."employee_id") 
            INNER JOIN "auth_user" ON ("queries_employee"."user_id" = "auth_user"."id") 
            WHERE "queries_restaurant_employees"."restaurant_id" IN (72, 73)
        """
        with self.assertNumQueries(3):
            for boss in bosses:
                print(boss.user)
                for restaurant in boss.restaurant_set.all():
                    for employee in restaurant.employees.all():
                        print(employee.user)
