from django.contrib.auth.models import User
from django.db import models


class Employee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    restaurants = models.ManyToManyField('Restaurant')


class Boss(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)


class Restaurant(models.Model):
    name = models.CharField(max_length=100)
    employees = models.ManyToManyField(Employee)
    boss = models.ForeignKey(Boss, on_delete=models.PROTECT)

    def __str__(self):
        return str(self.name)


